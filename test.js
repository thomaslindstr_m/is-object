// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isObject = require('./index.js');
    var assert = require('assert');

    describe('is-object', function() {
        it('should return true for objects', function () {
            assert.equal(isObject({}), true);
        });

        it('should return false for strings', function () {
            assert.equal(isObject(''), false);
        });

        it('should return false for functions', function () {
            assert.equal(isObject(function () {}), false);
        });

        it('should return false for arrays', function () {
            assert.equal(isObject([]), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isObject(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isObject(null), false);
        });

        it('should return false for false', function () {
            assert.equal(isObject(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isObject(true), false);
        });
    });
