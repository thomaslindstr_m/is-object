# is-object

[![build status](https://gitlab.com/ci/projects/107901/status.png?ref=master)](https://gitlab.com/ci/projects/107901?ref=master)

object type checker

```
npm install @amphibian/is-object
```

```javascript
var isObject = require('@amphibian/is-object');

isObject({}); // > true
isObject([]); // > false
```
